resource "tls_private_key" "tls_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = "ssh_key"
  public_key = "${tls_private_key.tls_key.public_key_openssh}"

  provisioner "local-exec" { # Create "ssh_key.pem" to your computer!!
    command = "echo '${tls_private_key.tls_key.private_key_pem}' > ./ssh_key.pem"
  }
}

provider "aws" {

  region = "us-east-1"
  profile = "default"
}

resource "aws_vpc" "bastion-host-vpc" {

  cidr_block = "192.168.0.0/16"
  instance_tenancy = "default"
  enable_dns_hostnames = "true"
  tags = {
    Name = "bastion-host-vpc"
  }
}

resource "aws_subnet" "public-subnet-us-east-1a" {
  vpc_id = aws_vpc.bastion-host-vpc.id
  cidr_block = "192.168.0.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "public-subnet-us-east-1a"
  }
}

resource "aws_subnet" "private-subnet-us-east-1b" {
  vpc_id = aws_vpc.bastion-host-vpc.id
  cidr_block = "192.168.1.0/24"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = "false"
  tags = {
    Name = "private-subnet-us-east-1b"
  }
}

resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.bastion-host-vpc.id

  tags = {
    Name = "Bastion-Host-Internet-gateway"
  }

}

# Elastic-IP (eip) for NAT
resource "aws_eip" "nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.IGW
  ]
}

# NAT
resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public-subnet-us-east-1a.id

  tags = {
    Name        = "Bastion-nat-gateway"
  }
}

resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.bastion-host-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.IGW.id

  }

  tags = {
    Name = "Route Table"
  }
}

# Routing tables to route traffic for Private Subnet
resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.bastion-host-vpc.id

  tags = {
    Name        = "Private-Route-Table"
  }
}

# Route for NAT
resource "aws_route" "private_nat_gateway" {
  route_table_id         = aws_route_table.private-route-table.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat.id
}

# Route table associations for Public Subnets
resource "aws_route_table_association" "rta" {
  subnet_id = aws_subnet.public-subnet-us-east-1a.id
  route_table_id = aws_route_table.route_table.id
}

# Route table associations for Private Subnets
resource "aws_route_table_association" "private-rta" {
  subnet_id = aws_subnet.private-subnet-us-east-1b.id
  route_table_id = aws_route_table.private-route-table.id
}

resource "aws_security_group" "sg1" {
  name = "allow_http_ssh_icmp"
  vpc_id = aws_vpc.bastion-host-vpc.id

  ingress {
    description = "Allow_http"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow_icmp"
    from_port = 0
    to_port = 0
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow_ssh"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http_icmp_ssh"
  }
}

resource "aws_security_group" "sg2" {

  name = "allow_http_ssh"
  vpc_id = aws_vpc.bastion-host-vpc.id

  ingress {
    description = "Allow_db"
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {

    Name = "allow_db"
  }

}

resource "aws_security_group" "sg3" {

  name = "allow_bastion_os"
  vpc_id = aws_vpc.bastion-host-vpc.id

  ingress {
    description = "Allow_ssh"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow bastion os"
  }
}

resource "aws_security_group" "sg4" {

  name = "allow private subnet jump"
  vpc_id = aws_vpc.bastion-host-vpc.id

  ingress {
    description = "Allow_ssh"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = [aws_security_group.sg3.id]

  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Name = "allow private access"
  }


}

# > Lets create 2 EC2 instance to test the architecture above created;

#resource "aws_instance" "Web_public" {
#
#  ami = "ami-0022f774911c1d690"
#  instance_type = "t2.micro"
#  key_name = "${aws_key_pair.generated_key.key_name}"
#  vpc_security_group_ids = [aws_security_group.sg1.id]
#  subnet_id = aws_subnet.sub-1a.id
#
#  tags = {
#    Name = "WEB INTERFACE"
#  }
#
#}

resource "aws_instance" "Web_back_end" {

  ami = "ami-0022f774911c1d690"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.generated_key.key_name}"
  vpc_security_group_ids = [aws_security_group.sg2.id,aws_security_group.sg4.id]
  subnet_id = aws_subnet.private-subnet-us-east-1b.id

  tags = {
    Name = "BACK-END-API"
  }

}

resource "aws_instance" "Web_bastion" {

  ami = "ami-0022f774911c1d690"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.generated_key.key_name}"
  vpc_security_group_ids = [aws_security_group.sg1.id]
  subnet_id = aws_subnet.public-subnet-us-east-1a.id

  tags = {
    Name = "BASTION HOST"
  }

}

output "key" {
  value = "${aws_key_pair.generated_key.public_key}"
}

output "ec2_global_ips" {
  value = ["${aws_instance.Web_bastion.*.public_ip}"]
}