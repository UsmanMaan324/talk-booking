from fastapi import FastAPI

app = FastAPI()


@app.get("/health-check")
def health_check():
    return {"message": "In good health"}


@app.get("/")
def health_check():
    return {"message": "home"}